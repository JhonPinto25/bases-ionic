import { Component, OnInit } from '@angular/core';

interface Componente {
  icon : string,
  name: string
  redirecTo: string
}

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage  {

  constructor() { }


  componentes : Componente[] = [
    {
      icon: 'airplane',
      name: 'nosotros',
      redirecTo: '/nosotros',

    },
    {
      icon: 'american-football-outline',
      name: 'alerta',
      redirecTo: '/alerta',

    },
    {
      icon: 'bicycle',
      name: 'Action-Sheet',
      redirecTo: '/action-sheet',

    },
    {
      icon: 'body',
      name: 'avatar',
      redirecTo: '/avatar',

    },
    {
      icon: 'thumbs-up',
      name: 'Botones',
      redirecTo: '/botones',

    },
    {
      icon: 'accessibility',
      name: 'Contactos',
      redirecTo: '/contactos',

    },
  ];
}
